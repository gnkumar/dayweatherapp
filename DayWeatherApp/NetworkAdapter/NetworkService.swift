//
//  NetworkService.swift
//  MQCodeChallenge
//
//  Created by Apple on 21/09/21.
//

import Foundation
import CoreLocation

class NetworkService {
    
    static let shared = NetworkService()
    let BaseURL = "https://api.openweathermap.org/data/2.5/onecall?"
    let URL_API_KEY = "65d00499677e59496ca2f318eb68c049"
    let session = URLSession(configuration: .default)
    //https://api.openweathermap.org/data/2.5/onecall?lat=33.44&lon=-94.04&exclude=hourly,daily&appid=65d00499677e59496ca2f318eb68c049
    // https://api.openweathermap.org/data/2.5/onecall/timemachine?lat={lat}&lon={lon}&dt={time}&appid={API key}

    
    func getDailyWeatherList(location: CLLocation, onSuccess: @escaping (DailyWeatherList) -> Void, onError: @escaping (String) -> Void) {
        guard let url = URL(string: "\(BaseURL)lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&exclude=hourly,daily&appid=\(URL_API_KEY)") else {
            onError("Error building URL")
            return
        }
        print(url)
        let task = session.dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }
                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }
                do {
                    if response.statusCode == 200 {
                        print(response.statusCode)
                        let items = try JSONDecoder().decode(DailyWeatherList.self, from: data)
                        print(items)
                        onSuccess(items)
                    } else {
                        onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                    }
                } catch {
                    onError(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    func getDayWeatherList(location: CLLocation, dt: Int, onSuccess: @escaping (DayWeater) -> Void, onError: @escaping (String) -> Void) {
        guard let url = URL(string: "\(BaseURL)lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&dt=\(dt)&appid=\(URL_API_KEY)") else {
            onError("Error building URL")
            return
        }
        print(url)
        let task = session.dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }
                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }
                do {
                    if response.statusCode == 200 {
                        print(response.statusCode)
                        let items = try JSONDecoder().decode(DayWeater.self, from: data)
                        print(items)
                        onSuccess(items)
                    } else {
                        onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                    }
                } catch {
                    onError(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
   /* func getWeather(location: CLLocation, onSuccess: @escaping (Result) -> Void, onError: @escaping (String) -> Void) {
        guard let url = URL(string: "\(BaseURL)lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)&appid=\(URL_API_KEY)") else {
            onError("Error building URL")
            return
        }
        print(url)
        let task = session.dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }
                
                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }
                
                do {
                    if response.statusCode == 200 {
                        print(response.statusCode)
                        let items = try JSONDecoder().decode(Result.self, from: data)
                        print(items)
                        onSuccess(items)
                    } else {
                        onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                    }
                } catch {
                    onError(error.localizedDescription)
                }
            }
            
        }
        task.resume()
    }*/
}
   
