//
//  WeatherViewModel.swift
//  MQCodeChallenge
//
//  Created by Apple on 20/09/21.
//

import Foundation
import CoreLocation

class WeatherViewModel : NSObject {
    
    private var apiService : NetworkService!
    public var data: CLLocation!
    public var dt: Int!
    public var error: String!

    private(set) var dailyWeatherList : DailyWeatherList? {
        didSet {
            self.bindWeatherViewModelToController()
        }
    }
    
    private(set) var dayWeather : DayWeater? {
        didSet {
            self.bindDayWeaterViewModelToController()
        }
    }
   
    var bindWeatherViewModelToController : (() -> ()) = {}
    var bindDayWeaterViewModelToController: (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService =  NetworkService()
    }
    
    func apiCalling()  {
        callFuncToGetWeatherData(location: self.data)
    }
    
    func callFuncToGetWeatherData(location: CLLocation) {
        self.apiService.getDailyWeatherList(location: location) { (result) in
            self.dailyWeatherList = result
        } onError: { (res) in
            print(res)
            self.error = res
        }
    }
    
    func apiDayWeatherCalling()  {
        callDayWeather(location: self.data, selectedDay: dt)
    }
    
    func callDayWeather(location: CLLocation, selectedDay: Int) {
        self.apiService.getDayWeatherList(location: location, dt: selectedDay) { (result) in
            self.dayWeather = result
        } onError: { (res) in
            print(res)
            self.error = res
        }
    }
    
    
}
