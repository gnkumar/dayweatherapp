//
//  DayWeatherView.swift
//  DayWeatherApp
//
//  Created by Apple on 21/09/21.
//

import UIKit
import CoreLocation

class DayWeatherView: BaseViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    @IBOutlet weak var labelWindInformation: UILabel!
    @IBOutlet weak var labelHumidity: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    let locationManager = CLLocationManager()
    private let weatherViewModel = WeatherViewModel()
    private var activityIndicator = UIActivityIndicatorView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Weather"
        tableView.isHidden = true
        self.activityIndicator = UIActivityIndicatorView.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        self.activityIndicator.center = CGPoint(x: view.bounds.width/2, y: view.bounds.height/2)
        view.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        locationAccessRequest()
        // Do any additional setup after loading the view.
    }
   
    private func locationAccessRequest() {
        locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
    }
    
    @objc func showWeatherInfo() {
        locationManager.startUpdatingLocation()
    }
    
    func dateToUnixTimeStamp(date: Date) {
        let unixtime = date.timeIntervalSince1970
        let unixTimeInString = String(unixtime)
        let splitText = unixTimeInString.split(separator: ".")
        print(splitText[0])
    }
    
    func serviceCalling()  {
        activityIndicator.startAnimating()
        weatherViewModel.apiCalling()
        weatherViewModel.bindWeatherViewModelToController = {
            DispatchQueue.main.async {
                guard let mainData = self.weatherViewModel.dailyWeatherList else {
                    Toast.show(message: "Something went wrong...", controller: self)
                    return
                }
                
                let temp = mainData.current.temp
                let humidity = mainData.current.humidity
                let windInfo = mainData.current.windSpeed
                let celsiusTemp = temp - 273.15
                let celsiusString = String(format: "%.0f",celsiusTemp)
                self.labelWindInformation.text = "\(windInfo)"
                self.labelHumidity.text = "\(humidity)"
                self.tempLabel.text = "\(celsiusString)"
                self.tableView.isHidden = false
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()
            }
        }
    }
    func callDayWeatherInfCalling() {
        activityIndicator.startAnimating()
        weatherViewModel.apiDayWeatherCalling()
        weatherViewModel.bindDayWeaterViewModelToController = {
            DispatchQueue.main.async {
                guard let mainData = self.weatherViewModel.dayWeather else {
                    Toast.show(message: "Something went wrong...", controller: self)
                    return
                }
                let temp = mainData.current.temp
                let humidity = mainData.current.humidity
                let windInfo = mainData.current.windSpeed
                let celsiusTemp = temp - 273.15
                let celsiusString = String(format: "%.0f",celsiusTemp)
                print(windInfo)
                print(celsiusString)
                self.labelWindInformation.text = "\(windInfo)"
                self.labelHumidity.text = "\(humidity)"
                self.tempLabel.text = "\(celsiusString)"
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()
            }
        }
    }

    func loader() -> UIAlertController {
            let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.large
            loadingIndicator.startAnimating()
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
            return alert
        }
        
        func stopLoader(loader : UIAlertController) {
            DispatchQueue.main.async {
                loader.dismiss(animated: true, completion: nil)
            }
        }
    
}

extension DayWeatherView: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard locations.count > 0 else {
            return
        }
        weatherViewModel.data = locations.first
        self.tableView.isHidden = true
        serviceCalling()
       // locationManager.stopUpdatingLocation()
    }
}

extension DayWeatherView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.weatherViewModel.dailyWeatherList?.minutely?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        let unixDate = weatherViewModel.dailyWeatherList?.minutely?[indexPath.row].dt
        let epocTime = TimeInterval(unixDate ?? 0)
        let myDate = NSDate(timeIntervalSince1970: epocTime)
        cell?.textLabel?.text = "\(myDate)"
        return cell ?? UITableViewCell()
    }

}

extension DayWeatherView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let unixDate = weatherViewModel.dailyWeatherList?.minutely?[indexPath.row].dt
        weatherViewModel.dt = unixDate
        callDayWeatherInfCalling()
    }
}
