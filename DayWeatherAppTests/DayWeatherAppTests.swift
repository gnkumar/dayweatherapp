//
//  DayWeatherAppTests.swift
//  DayWeatherAppTests
//
//  Created by Apple on 21/09/21.
//

import XCTest
import CoreLocation
@testable import DayWeatherApp

class DayWeatherAppTests: XCTestCase {

   var viewModel = WeatherViewModel()
    
    func test_callDayListService() {
        let location = CLLocation.init(latitude: 17.987, longitude: 68.7635)
        viewModel.callFuncToGetWeatherData(location: location)
        viewModel.callDayWeather(location: location, selectedDay: 12342)
    }

}
